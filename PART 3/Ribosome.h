#pragma once
#include "Protein.h"
#define LENGTH_OF_CODON 3 

class Ribosome
{
public:
	/**create protein from the RNA transcript**/
	Protein * create_protein(std::string &RNA_transcript) const;
};