#include "Ribosome.h"

/**create protein from the RNA transcript**/
Protein * Ribosome::create_protein(std::string &RNA_transcript) const
{
	std::string codon = "";
	AminoAcid amino_acid = UNKNOWN;
	Protein * list = new Protein;

	list->init();
	unsigned int rna_length = RNA_transcript.length();
	if (rna_length >= LENGTH_OF_CODON)
	{
		for (size_t i = 0; i <= rna_length - LENGTH_OF_CODON; i+= LENGTH_OF_CODON)
		{
			codon = RNA_transcript.substr(i, LENGTH_OF_CODON);
			amino_acid = get_amino_acid(codon);
			if (amino_acid == AminoAcid::UNKNOWN)
			{
				list->clear();
			}
			else
			{
				list->add(amino_acid);
			}
		}
		return list;
	}
	else
	{
		return nullptr;
	}
}