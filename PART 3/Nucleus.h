#pragma once
#include <string>
#include <iostream>

/** A short class that represents a Gene*/
class Gene
{
public:
	/*initialize the Gene*/
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	//getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand()const;

	// setters
	void set_start(unsigned int start);
	void set_end(unsigned int end);
	void set_on_complementary_dna_strand(unsigned int on_complementary_dna_strand);

private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
};

/** A short class that represents a Nucleus*/
class Nucleus
{
public:
	//initialize the complementary D.N.A sequence
	void init(const std::string dna_sequence);
	 
	//**returns a R.N.A transcript corresponding to a particular gene**/
	std::string get_RNA_transcript(const Gene& gene) const;

	/**Returns the opposite of the first DNA strand held in the cell nucleus**/
	std::string get_reversed_DNA_strand() const;

	/**Returns the number of times a particular codon appears on the first DNA strand held in the cell nucleus**/
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
};
