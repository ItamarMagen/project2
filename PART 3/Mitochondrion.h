#pragma once
#include "Protein.h"
#include "AminoAcid.h"
#define MIN_GLOCUSE_LEVEL 50

class Mitochondrion
{
public:
	/**initialize the Mitochondrion**/
	void init();
	/**The function receives a protein and checks whether it is from the correct configuration**/
	void insert_glocuse_receptor(const Protein & prote);
	/**Changes the amount of glucose**/
	void set_glucose(const unsigned int glocuse_units);
	/**Checks if the mitochondria can produce atp, if it returns true**/
	bool produceATP(const int glocuse_unit) const;
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};

