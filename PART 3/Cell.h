#pragma once

#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"
#include "Protein.h"

/** A short class that represents a Cell*/
class Cell
{
public:
	/**initialize the Cell**/
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	/**A function that loads the cell in energy**/
	bool get_ATP();

private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};

