#include "Cell.h"

/**initialize the Cell**/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
	this->_nucleus.init(dna_sequence);
	this->_ribosome;
	this->_atp_units = 0;
}

/**A function that loads the cell in energy**/
bool Cell::get_ATP()
{
	bool canCreateAtp = false;
	std::string rna = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein * protein = _ribosome.create_protein(rna);
	if (protein == nullptr)
	{
		std::cerr << "The ribosome can't create the protein";
		_exit(1);
	}
	else
	{
		_mitochondrion.insert_glocuse_receptor(*protein);
		_mitochondrion.set_glucose(50);
		canCreateAtp = _mitochondrion.produceATP(50);
		if (canCreateAtp)
		{
			this->_atp_units = 100;
			return true;
		}
		else
		{
			return false;
		}
	}
}