#include "Mitochondrion.h"

/**initialize the Mitochondrion**/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

/**The function receives a protein and checks whether it is from the correct configuration**/
void Mitochondrion::insert_glocuse_receptor(const Protein & prote)
{
	AminoAcidNode * curr = prote.get_first();
	if (curr->get_data() == AminoAcid::ALANINE)
	{
		curr = curr->get_next();
		if (curr->get_data() == AminoAcid::LEUCINE)
		{
			curr = curr->get_next();
			if(curr->get_data() == AminoAcid::GLYCINE)
			{
				curr = curr->get_next();
				if (curr->get_data() == AminoAcid::HISTIDINE)
				{
					curr = curr->get_next();
					if (curr->get_data() == AminoAcid::LEUCINE)
					{
						curr = curr->get_next();
						if (curr->get_data() == AminoAcid::PHENYLALANINE)
						{
							curr = curr->get_next();
							if (curr->get_data() == AminoAcid::AMINO_CHAIN_END)
							{
								this->_has_glocuse_receptor = true;
							}
						}
					}
				}
			}
		}
	}
}

/**Changes the amount of glucose**/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

/**Checks if the mitochondria can produce atp, if it returns true**/
bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	if (this->_has_glocuse_receptor == true && glocuse_unit >= MIN_GLOCUSE_LEVEL)
		return true;
	else
		return false;
}