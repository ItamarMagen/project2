#include "Nucleus.h"

/*initialize the Gene*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

// getters
unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand()const
{
	return this->_on_complementary_dna_strand;
}

// setters
void Gene::set_start(unsigned int start)
{
	this->_start = start;
}

void Gene::set_end(unsigned int end)
{
	this->_start = end;
}

void Gene::set_on_complementary_dna_strand(unsigned int on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

/** initialize the complementary D.N.A sequence*/
void Nucleus::init(const std::string dna_sequence)
{
	std::string newString = "";
	this->_DNA_strand = dna_sequence;
	for (size_t i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'G')
			newString += 'C';
		else if (dna_sequence[i] == 'C')
			newString += 'G';
		else if (dna_sequence[i] == 'A')
			newString += 'T';
		else if (dna_sequence[i] == 'T')
			newString += 'A';
		else
		{
			std::cerr << "One of the letters in the sequence isn't valid, only the letters A,C,G and T are valid\n";
			_exit(1);
		}
	}
	this->_complementary_DNA_strand = newString;
}

/**returns a R.N.A transcript corresponding to a particular gene**/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string dna = "";
	std::string rna = "";
	unsigned int start = gene.get_start();
	unsigned int end = gene.get_end();
	bool on_complementary_dna_strand = gene.is_on_complementary_dna_strand();

	if (on_complementary_dna_strand)//==True
		dna = Nucleus::_complementary_DNA_strand;
	else
		dna = Nucleus::_DNA_strand;
	
	for (size_t i = start; i <= end; i++)
	{
		if (dna[i] == 'T')
			rna	+= 'U';
		else
			rna += dna[i];
	}
	return rna;
}

/**Returns the opposite of the first DNA strand held in the cell nucleus**/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversed_dna = "";
	std::string original_dna = Nucleus::_DNA_strand;
	unsigned int dna_length = original_dna.length();
	for (int i = dna_length - 1; i >= 0; i--)
	{
		reversed_dna += original_dna[i];
	}
	return reversed_dna;
}

/**Returns the number of times a particular codon appears on the first DNA strand held in the cell nucleus**/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int count = 0;
	std::string temp_str = "";
	std::string original_dna = Nucleus::_DNA_strand;
	unsigned int original_dna_length = original_dna.length();
	for (size_t i = 0; i <= original_dna_length - 3; i++)
	{
		temp_str = original_dna[i] + original_dna[i+1] + original_dna[i+2];
		if (temp_str == codon)
			count++;
	}
	return count;
}